from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm


def show_todo_list_list(request):
    todo_list_list = TodoList.objects.all()
    context = {
        "todo_list_list": todo_list_list,
    }
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    # items = TodoList.objects.get(id=id)
    details = get_object_or_404(TodoList, id=id)
    context = {"details": details}
    return render(request, "todos/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        # use form to validate values and save to database
        form = TodoListForm(request.POST)
        if form.is_valid():
            # save all information from form
            form = form.save()
            # if all is well, redirect browser to other page and leave function
            return redirect("todo_list_detail", id=form.id)
    else:
        # create an instance of the Django model form class
        form = TodoListForm()
        # put the form in the context
        context = {"form": form}
        return render(request, "todos/create.html", context)


def todo_list_update(request, id):
    # bring in the current data for this instance
    todolist = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        # use form to replace and validate values and save to database
        form = TodoListForm(request.POST, instance=todolist)
        if form.is_valid():
            form.save()
            # if all is well, redirect browser to other page and leave function
            return redirect("todo_list_detail", id)
    else:
        # bring up the instance of the Django model form class
        form = TodoListForm(instance=todolist)
        # put the form and recipe in the context
        context = {"todolist": todolist, "form": form}
        return render(request, "todos/edit.html", context)


def todo_list_delete(request, id):
    todolist = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        todolist.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")


def todo_item_create(request):

    if request.method == "POST":
        # use form to validate values and save to database
        form = TodoItemForm(request.POST)
        if form.is_valid():
            # save all information from form
            item = form.save()
            # if all is well, redirect browser to other page and leave function
            return redirect("todo_list_detail", id=item.list.id)
    else:
        # create an instance of the Django model form class
        form = TodoItemForm()
        todo_list_list = TodoList.objects.all()
        # put the form in the context
        context = {
            "form": form,
            "todo_list_list": todo_list_list,
        }
    return render(request, "items/create.html", context)


def todo_item_update(request, id):
    # bring in the current data for this instance
    todolistitem = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        # use form to replace and validate values and save to database
        form = TodoItemForm(request.POST, instance=todolistitem)
        if form.is_valid():
            form.save()
            # if all is well, redirect browser to other page and leave function
            return redirect("todo_list_detail", id)
    else:
        # create an instance of the Django model form class
        form = TodoItemForm(instance=todolistitem)
        todo_list_list = TodoList.objects.all()
        # put the form in the context
        context = {
            "form": form,
            "todo_list_item": todolistitem,
            "todo_list_list": todo_list_list,
        }
        print(context, "Context")
        return render(request, "items/edit.html", context)


# todolistitem.todolist.id
